---
layout: markdown_page
title: "Performance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Other Related Pages
{:.no_toc}

- [GitLab.com (infra) architecture](handbook/infrastructure/production-architecture/)
- [Monitoring GitLab.com](handbook/infrastructure/monitoring/)
- [Application Architecture documentation](https://docs.gitlab.com/ce/development/architecture.html)
- [GitLab.com Settings](https://about.gitlab.com/gitlab-com/settings/)
- [GitLab performance monitoring documentation](https://docs.gitlab.com/ce/administration/monitoring/performance/introduction.html)

## Standards we use to measure performance

To clarify what we mean when discussing performance of GitLab, we use the following categories:
- [First Paint](https://developers.google.com/web/tools/lighthouse/audits/first-meaningful-paint):
"the time at which [...] the primary content of the page is visible.".
- [First Byte](https://en.wikipedia.org/wiki/Time_To_First_Byte): the time
between making a web request and receiving the first byte of information in return.

For each category, the following modifiers can be applied:
- User: how a _real_  GitLab user would experience and measure the time.
- Internal: the time as measured from _inside_  GitLab.com's infrastructure (the
  boundary is defined as being at the "network | Azure load balancer" interface).
- External: the time as measured from any specified point outside GitLab.com's
infrastructure; for example a DO box with Prometheus monitoring or a browser in a specified geo-region on a specified network speed.

The odd case in this set would be "First Paint - Internal"; but this can probably
be measured by setting up a headless browser either at or very near the Azure
load balancer.

### First Paint
{: #first-paint}

Performance of GitLab and GitLab.com is ultimately about the user experience. As
also described in the [product management handbook](https://about.gitlab.com/handbook/product/#performance),
"faster applications are better applications".

Since the speed of the application depends on the usage of it, we've decided to
use heavy use cases as the basis for measuring performance. Specifically, the
URLs from GitLab.com listed in the table below form the basis for measuring
performance improvements. The times indicate from web request to "First Paint",
and are noted in milliseconds. (`BoQ` is the timing at the beginning of the
  current quarter; `EoQ` is the target for the end of the current quarter).
  Times are measured from a single geo-location (in Europe) using native
  connectivity for that location - we will call this "First Paint - External"
  though since the "User" in this case is well controlled.

| Type | URL | BoQ | EoQ |
|------|-----|----:|----:|
|Issue | <https://gitlab.com/gitlab-org/gitlab-ce/issues/4058> |  [6079](http://207.154.197.115/gl/sitespeed-result/gitlab.com/2017-06-27-08-18-14/pages/gitlab.com/gitlab-org/gitlab-ce/issues/4058/index.html)  | 5000|
| Merge request | <https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9546> | [8519](http://207.154.197.115/gl/sitespeed-result/gitlab.com/2017-06-27-08-18-14/pages/gitlab.com/gitlab-org/gitlab-ce/merge_requests/9546/index.html) | 5000 |
| Pipeline | <https://gitlab.com/gitlab-org/gitlab-ce/pipelines/9360254> | [9664](http://207.154.197.115/gl/sitespeed-result/gitlab.com/2017-06-27-08-18-14/pages/gitlab.com/gitlab-org/gitlab-ce/pipelines/9360254/index.html) | 5000 |
| Repo | <http://gitlab.com/gitlab-org/gitlab-ce/tree/master> | [2640](http://207.154.197.115/gl/sitespeed-result/gitlab.com/2017-06-29-07-44-06/pages/gitlab.com/gitlab-org/gitlab-ce/tree/master/index.html) | 5000 |

### First Byte
{: #first-byte}

Time To First Byte ([TTFB](https://en.wikipedia.org/wiki/Time_To_First_Byte))
performance measures the time between making a web request and receiving the
first byte of information in return. As a result, First Byte encompasses everything
that is the backend as well as network transit issues. It differs from the
_First Paint_ mostly by frontend related issues such as javascript
loading, page rendering, and so on (for more details, see the
  [flow of a web request](#flow-of-web-request) below).

Timing history and targets for TTFB are listed below (click on the tachometer
  icons for _current_ timings (First Byte - External)):

| Type | URL | BoQ | Now | EoQ |
|------|-----|----:|----:|----:|
|Issue | <https://gitlab.com/gitlab-org/gitlab-ce/issues/4058> |  [3693](http://207.154.197.115/gl/sitespeed-result/gitlab.com/2017-06-27-08-18-14/pages/gitlab.com/gitlab-org/gitlab-ce/issues/4058/index.html) | [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/gitlab-web-status?refresh=1m&panelId=1&fullscreen&orgId=1)  | 1000|
| Merge request | <https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9546> | [6347](http://207.154.197.115/gl/sitespeed-result/gitlab.com/2017-06-27-08-18-14/pages/gitlab.com/gitlab-org/gitlab-ce/merge_requests/9546/index.html) | [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/gitlab-web-status?refresh=1m&panelId=25&fullscreen&orgId=1)  | 1000 |
| Pipeline |<https://gitlab.com/gitlab-org/gitlab-ce/pipelines/9360254> | [2987](http://207.154.197.115/gl/sitespeed-result/gitlab.com/2017-06-27-08-18-14/pages/gitlab.com/gitlab-org/gitlab-ce/pipelines/9360254/index.html) | [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/gitlab-web-status?refresh=1m&panelId=20&fullscreen&orgId=1)  | 1000 |
| Repo | <http://gitlab.com/gitlab-org/gitlab-ce/tree/master> | [1080](http://207.154.197.115/gl/sitespeed-result/gitlab.com/2017-06-29-07-44-06/pages/gitlab.com/gitlab-org/gitlab-ce/tree/master/index.html) | [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/gitlab-web-status?refresh=1m&panelId=28&fullscreen&orgId=1)  | 1000 |

#### First Byte - Internal
{: #first-byte-internal}

To go a little deeper and measure performance of the application & infrastructure without
consideration for frontend and network aspects, we look at "transaction timings"
[as recorded by Unicorn](#unicorn2various). These timings can be seen on the
[Rails Controller dashboard](https://performance.gitlab.net/dashboard/db/rails-controllers?orgId=1&var-action=Projects::MergeRequestsController%23show&var-database=Production) _per URL that is accessed_ .

For instance, to get the transaction timing for the merge request referenced
above first visit the merge request page, then visit the Rails Controller
dashboard and scroll down to the [Transaction Details table](https://performance.gitlab.net/dashboard/db/rails-controllers?panelId=11&fullscreen&orgId=1&var-action=Projects::MergeRequestsController%23show&var-database=Production).
We do not currently have time series graphs _per URL_ nor do we have specific targets in terms of what this timing should be.


## Flow of information in various scenarios, and its performance

All items that start with the <i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>
symbol represent a step in the flow that we _measure_ . Wherever possible, the
tachometer icon links to the relevant dashboard in our [monitoring](handbook/infrastructure/monitoring/).
Also take a look at some recently added User perspective times along with a
[breakdown of the steps involved on the frontend](http://207.154.197.115/gl/sitespeed-result/gitlab.com/)

### Flow of web request
{: #flow-of-web-request}

Considering the scenarios of a user opening their browser, and surfing to their dashboard
by typing `gitlab.com/dashboard`, here is what happens:

1. **User request reaches backend** <a name="request-reaches-BE"></a>
    1. <a name="start-request"></a> User enters gitlab.com/dashboard in their browser and hits enter
    1. <a name="lookup-IP"></a> Browser looks up IP address in DNS server
       - DNS request goes out and comes
    back (typically ~10-20 ms, [data?]; often times it is already cached so
      then it would be faster).
       - For more details on the steps from browser to application, enjoy reading <https://github.com/alex/what-happens-when>
       - Opportunities:
          - We use Route53 for DNS, and will start using DynDNS soon as well. [Related issue: infrastructure#1711](https://gitlab.com/gitlab-com/infrastructure/issues/1711).
       - not measured
    1. <a name="browser2AzLB"></a> From browser to Azure load balancer
       - Now that the browser knows where to find the IP address, browser sends the web
    request (for gitlab.com/dashboard) to Azure.
       - not measured
1. **Backend processes request and returns to browser** <a name="backend-processes"></a>
    1. <a name="AzLB2HAProxy"></a> Azure's LB to HAProxy
       - Azure's load balancer determines where to route the packet (request), and
       sends the request to our Frontend Load Balancer(s) (also referred to as
         HAProxy).
       - not measured
    1. <a name="HAProxy-SSL"></a> HAProxy (load balancer) does SSL negotiation with the browser (takes time)
       - not measured
       - [todo, follow up on "there are some Web settings that may help: <https://linux-audit.com/optimize-ssl-tls-for-maximum-security-and-speed/>"]
    1. <a name="HAProxy2NGINX"></a> HAProxy forwards to NGINX in one of our front end workers
       - In this case, since we are tracking a web request, it would be the nginx box in the
         "Web" box in the production-architecture diagram; but alternatively the request can come in via API or a git command
         from the command line, hence the API, and git "boxes")
        - Since all of our servers are in ONE Azure VNET, the overhead of SSL
          handshake and teardown between HAProxy and NGINX should be close to negligible.
        - not measured
    1. <a name="NGINX-buffer"></a> NGINX gathers all network packets related to the request ("request buffering")
       - the request may be split into multiple packets by the intervening network,
       for more on that, read up on [MTUs](https://en.wikipedia.org/wiki/Maximum_transmission_unit).
       - In other flows, this won't be true. Specifically, request buffering is
       [switched off for LFS](https://gitlab.com/gitlab-org/gitlab-workhorse/issues/130).
       - not measured, and not in our control.
    1. <a name="NGINX2workhorse"></a> NGINX forwards full request to workhorse (in one combined request)
       - not measured
    1. <a name="workhorse2various"></a> Workhorse splits the request into parts to forward to
       - <a name="workhorse2unicorn"></a> [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=13&fullscreen&orgId=1)Unicorn (time spent waiting for Unicorn to pick up a request = `HTTP queue
          time`).
       - <a name="workhorse2gitaly"></a> [not in this scenario, but not measured in any case] Gitaly
       - <a name="workhorse2nfs"></a> [not in this scenario, but not measured in any case] NFS (git clone through HTTP)
       - <a name="workhorse2redis"></a> [not in this scenario, but not measured in any case] Redis (long polling)
    1. <a name="unicorn2various"></a> [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=2&fullscreen&orgId=1) Unicorn processes request and returns to Workhorse
       - Unicorn, (often just called "Rails", or "application server"), translates
       the request into a Rails controller request; in this case `RootController#index`.
       - The round trip time it takes for a request to _start_  in Unicorn and _leave_ Unicorn
    is what we call `Transaction Timings`.
       - RailsController requests are sent to (and data is received from):
          - <a name="unicorn2db"></a> [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=9&fullscreen&orgId=1) PostgreSQL (`SQL timings`),
          - <a name="unicorn2nfs"></a> [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/daily-overview?panelId=14&fullscreen&orgId=1) NFS (`git timings`),
          - <a name="unicorn2redis"></a> [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/daily-overview?panelId=13&fullscreen&orgId=1) Redis (`cache timings`).
       - In this `gitlab.com/dashboard` example, the controller addresses all three [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/rails-controllers?orgId=1&var-action=RootController%23index&var-database=Production).
          - There are usually _multiple_ SQL calls (or file, or cache, etc.) calls for a given
      controller request. These add to the overall timing, especially since they are
      sequential. For example, in
      this scenario, there are [29 SQL calls (search for `Load`)](http://profiler.gitlap.com/20170524/901687e2-9fa1-4256-8414-c4835dc31dbc.txt.gz)
      when this _particular user_ hits `gitlab.com/dashboard/issues`. The number of SQL calls
      will depend on how many projects the person has, how much may already be in cache, etc.
          - There's generally no multi-tasking within a single Rails request. In a
       number of places we multi-task by serving a HTML page that uses AJAX to
       fill in some data, for example on `gitlab.com/username` the contribution
       calendar and the "most recent activity" sections are loaded in parallel.
          - In the Rails stack, middleware typically adds to the number of round trips
       to Redis, NFS, and PostgreSQL, per controller call, in addition to the
       timings of Rails controllers.  Middleware is used for {session state, user
      identity, endpoint authorization, rate limiting, logging, etc} while the
      controllers typically have at least one round trip for each of {retrieve
      settings, cache check, build model views, cache store, etc.}. Each such
      roundtrip _estimated_ to take < 10 ms.
       - <a name="unicorn-views"></a> [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=8&fullscreen&orgId=1)  Unicorn constructs the relevant html blob (view) to be served back to the user. (`view timings`).
         - In our gitlab.com/dashboard example, view timings p99 are in the multiple seconds
       with mean < 1s. This is partially in parallel with the
       prior steps as the views dictate what they need to call from PostgreSQL, Redis, etc.
         - A particular view in Rails will often be constructed from multiple partial
        views. These will be used from a template file, specified by the controller
        action, that is, itself, generally included within a layout template.
        Partials can include other partials. This is done for good code
        organization and reuse. As an example, when the _particular user_  from the
        example above loads `gitlab.com/dashboard/issues`, there are [56 nested / partial views rendered (search for `View::`)](http://profiler.gitlap.com/20170524/901687e2-9fa1-4256-8414-c4835dc31dbc.html.gz)
         - Partial views may be cached via various [Rails techniques](http://guides.rubyonrails.org/caching_with_rails.html), such as Fragment Caching. In addition,
         GitLab has a Markdown cache stored in the database that is used to speed up the conversion of Markdown to HTML.
         - GitLab renders a lot of the views in the backend (i.e. in Unicorn) vs.
       frontend. To see the split, use your browser's "inspect" tool and look at
       TTFB (time to first byte, this is the browser waiting to hear anything back,
         which is due to work happening in the backend) and compare it to the
         download time.
       - <a name="unicorn2workhorse"></a> Unicorn sends html blob back to workhorse
         - Some of these blobs are expensive to compute, and are sometimes hard-coded
      to be sent from Unicorn to Redis (i.e. to cache) once rendered.
    1. <a name="workhorse2NGINX"></a> Workhorse sends html blob to NGINX
      - not measured
    1. <a name="NGINX2HAProxy"></a> NGINX sends html blob to HAProxy
      - not measured
    1. <a name="HAProxy2AzLB"></a> HAProxy send blob to Azure load balancer
      - not measured
    1. <a name="AzLB2browser"></a> Azure load balancer sends blob to browser
1. **Frontend processes received HTML and renders page** <a name="frontend2interactive"></a>
    1. <a name="browser-firstbyte"></a> Browser receives `first byte`.
      - Depends on network speed
      - not measured
    1. <a name="browser-assets"></a> Browser receives additional assets, such as javascript bundles, CSS, images,
    and webfonts and starts incrementally parsing.
      - Depends on number and size of assets, as well as network speed. For _each_
      asset, there is a round-trip of
           - for cached assets: browser <i class="fa fa-long-arrow-right fa-fw"
           aria-hidden="true"></i> nginx <i class="fa fa-long-arrow-right fa-fw"
           aria-hidden="true"></i> nginx confirms cached asset is still valid <i
           class="fa fa-long-arrow-right fa-fw" aria-hidden="true"></i> browser
           - for non-cached or expired cached assets: browser <i class="fa
           fa-long-arrow-right fa-fw" aria-hidden="true"></i> unicorn <i class="fa
           fa-long-arrow-right fa-fw" aria-hidden="true"></i> unicorn grabs asset
           from web worker host local cache <i class="fa fa-long-arrow-right fa-fw"
         aria-hidden="true"></i> browser.
      - Scripts and stylesheets block page rendering by default.
      - Opportunities
            - Prevent scripts from blocking page rendering by deferring non-essential
         scripts with `<script defer>`, moving them to the bottom of the `<body>`,
         or lazy-loading them with webpack code-splitting. [Related issue: gitlab-ce#33391](https://gitlab.com/gitlab-org/gitlab-ce/issues/33391).
            - Move non-HTML assets to a CDN to cut out transfer time. Related issues: [Setting up a CDN, infrastructure#57](https://gitlab.com/gitlab-com/infrastructure/issues/57);
         [Using image resizing service in CDN, gitlab-ce#34364](https://gitlab.com/gitlab-org/gitlab-ce/issues/34364)
              - In a later iteration, we can pre-fetch the DNS for the CDN host. This
         speeds up the fetching of assets from the CDN.
             - Asynchronously fetch assets using `prefetch` or `preload` depending
       on the likelihood that the [asset will be needed on a given page](https://medium.com/reloading/preload-prefetch-and-priorities-in-chrome-776165961bbf),
       speeding up later parsing. [Related issue: gitlab-ce#33391](https://gitlab.com/gitlab-org/gitlab-ce/issues/33391).
       - not measured
    1. <a name="browser-compiles"></a> Browser compiles and evaluates Javascript within the page.
       - Opportunities
          - Embedded `<script>` tags both block the page rendering and prevent us
          from deferring any javascript that they depend on. These should be
          eliminated. [See related MR discussion](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9742).
       - not measured
    1. <a name="browser-postDOM"></a> Browser runs events that depend on `DOMContentLoaded` firing.
       - [`DOMContentLoaded`](https://developer.mozilla.org/en-US/docs/Web/Events/DOMContentLoaded)
       fires when the initial HTML document has been completely loaded and parsed,
        without waiting for stylesheets, images, and subframes to finish loading.
      - Various features are not activated until this event triggers,
      such as enabling a button to be pressed, or a scrollbar to be scrolled.
       - not measured
    1. <a name="end-of-process"></a> Browser completes rendering and page is fully interactive.
       - Total time is currently not measured!

---

#### The flow displayed as a table with timings

All times are reported in milliseconds.

Guide to this table:
- `# per request` : average number of times this step occurs per request. For
instance, an average "transaction" may require [0.2 SQL calls, 0.4 git calls, 1
call to cache](https://docs.google.com/spreadsheets/d/15mhXjwkx2lOXJps7lsp_o0zbwGSyOdYOTc8-McwBy0A/pubhtml),
and 30 nested views to be built.
- `p99 BoQ`: the p99 timing (in milliseconds) at the beginning of the current quarter
- `p99 Now`: link to the dashboard that displays the _current_ p99 timing
- `p99 EoQ`: the target for the p99 timing by the end of the current quarter
- Numbers in _italics_ are _per event_  and/or _in parallel_  with other timings,
and therefore do not sum to the (sub)totals. The non-italic numbers _do_ sum to the (sub)totals.
- Any number with an asterisk ( <sup> * </sup> ) is an estimate.

| Step                                                    | # per request | p99 BoQ | p99 Now | p99 EoQ goal | Issue links and impact |
|---------------------------------------------------------|--------------:|--------:|--------:|-------------:|------------------------|
| [**USER REQUEST REACHES BACKEND**](#request-reaches-BE) |               |         |         |              |                        |
| [Lookup IP in DNS](#lookup-IP)                          |     1         |10<sup>*</sup>| ? |10<sup>*</sup>|                        |
| [Browser to Azure LB](#browser2AzLB)                    |     1         |10<sup>*</sup>| ? |10<sup>*</sup>|                        |
| [**BACKEND PROCESSES AND RETURNS TO BROWSER**](#backend-processes) |    |         |         |              |                        |
|[Azure LB to HAProxy](#AzLB2HAProxy)                     |     1         |2<sup>*</sup>| ? |2<sup>*</sup>|                        |
|[HAProxy SSL with Browser](#HAProxy-SSL)                 |     1         |10<sup>*</sup>| ? |10<sup>*</sup>|                        |
|[HAProxy forwards to NGINX](#HAProxy2NGINX)              |     1         |2<sup>*</sup>| ? |2<sup>*</sup>|                        |
|[NGINX buffers request](#NGINX-buffer)                   |     1         |10<sup>*</sup>| ? |10<sup>*</sup>|                        |
|[NGINX forwards to Workhorse](#NGINX2workhorse)          |     1         |2<sup>*</sup>|  ? |2<sup>*</sup>|                        |
|[Workhorse distributes request](#workhorse2various)      |     1         |         |         |      | [Adding monitoring to workhorse](https://gitlab.com/gitlab-com/infrastructure/issues/2025) |
|<i class="fa fa-long-arrow-right fa-fw" aria-hidden="true"></i>[_Workhorse sends to Unicorn_](#workhorse2unicorn) | 1 | 18  | [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=13&fullscreen&orgId=1) | 10 | [Adding Unicorns](https://gitlab.com/gitlab-com/infrastructure/issues/1883) |
|<i class="fa fa-long-arrow-right fa-fw" aria-hidden="true"></i>[_Workhorse sends to Gitaly_](#workhorse2gitaly)   | |     | ?  |     |   |
|<i class="fa fa-long-arrow-right fa-fw" aria-hidden="true"></i>[_Workhorse sends to NFS_](#workhorse2nfs)         | |     | ?  |     |   |
|<i class="fa fa-long-arrow-right fa-fw" aria-hidden="true"></i>[_Workhorse send to Redis_](#workhorse2redis)      | |     | ?  |     |   |
|[Unicorn works and returns HTML to Workhorse](#unicorn2various)          |  1  |  2500       | [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=2&fullscreen&orgId=1)  |  1000 |     |
|<i class="fa fa-long-arrow-right fa-fw" aria-hidden="true"></i>[_Unicorn to Postgres and back_](#unicorn2db)      | | _250_ |[<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=9&fullscreen&orgId=1)| _100_ | [Speed up slow queries](https://gitlab.com/gitlab-org/gitlab-ce/issues/34535)  |
|<i class="fa fa-long-arrow-right fa-fw" aria-hidden="true"></i>[_Unicorn to NFS and back_](#unicorn2nfs)          | | _460_ | [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/daily-overview?panelId=14&fullscreen&orgId=1)  | _200_ | Move to Gitaly [fast](https://gitlab.com/gitlab-org/gitaly/issues/313) - [sample result](https://gitlab.com/gitlab-com/infrastructure/issues/1912#note_31368476) |
|<i class="fa fa-long-arrow-right fa-fw" aria-hidden="true"></i>[_Unicorn to Redis and back_](unicorn2redis)       | |  _18_ | [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/daily-overview?panelId=13&fullscreen&orgId=1) |     |   |
|<i class="fa fa-long-arrow-right fa-fw" aria-hidden="true"></i>[_Unicorn builds views_](#unicorn-views) |  | _1500_   | [<i class="fa fa-tachometer fa-fw" aria-hidden="true"></i>](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=8&fullscreen&orgId=1)  |  |       |
|<i class="fa fa-long-arrow-right fa-fw" aria-hidden="true"></i>[_Unicorn sends HTML to Workhorse_](#unicorn2workhorse) | 1 |  |  |  |  |
|[Workhorse sends to NGINX](#workhorse2NGINX)             |      1        | 2<sup>*</sup>| ? |2<sup>*</sup>|                        |
|[NGINX sends to HAProxy](#NGINX2HAProxy)                 |      1        |2<sup>*</sup>| ? |2<sup>*</sup>|                        |
|[HAProxy sends to Azure LB](#HAProxy2AzLB)               |      1        |2<sup>*</sup>| ? |2<sup>*</sup>|                        |
|[Azure LB sends to Browser](#AzLB2browser)               |      1        |20<sup>*</sup>| ? |20<sup>*</sup>|                        |
| **_Subtotal_** see [note 1](#note-blackbox)          |               |**3833** |         |              |                        |
|[**FRONTEND PROCESSES HTML AND RENDERS PAGE**](#frontend2interactive) |  |         |         |              |                        |
|[Browser receives first byte](#browser-firstbyte)        |               |         |         |              |                        |
|[Browser receives all assets](#browser-assets) (see [note 2](#note-fe-times)) |  |340|         |              | [Smarter asset loading](https://gitlab.com/gitlab-org/gitlab-ce/issues/33391)|
|[Browser compiles JS](#browser-compiles) (see [note](#note-fe-times)) |  |   710   |         |              |                        |
|[Browser runs post-DOM events](#browser-postDOM) (see [note 2](#note-fe-times))| |630|         |              |                        |
|**_TOTAL TIME_** (see [note 3](#note-total-time))          |               |         |         |              |                        |
|---------------------------------------------------------|---------------|---------|---------|--------------|------------------------|

**Notes:**
- 1\. <a name="note-blackbox"></a> Based on mean, p95, p99 of all _non-staging_
URL's measured in our [blackbox monitoring](https://performance.gitlab.net/dashboard/db/gitlab-web-status?refresh=1m&panelId=14&fullscreen&orgId=1&from=now-90d&to=now),
between 2017-03-30 and 2017-06-28.
- 2\. <a name="note-fe-times"></a> The 340ms, 710ms, and 630ms measurements here
are from a sample size of 1 for a specific merge request URL, on a specific
hardware configuration. For details, see [the related issue](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/11836#).
- 3\. <a name="note-total-time"></a> _TOTAL TIME_ is not equal to sum of all of above in the table, due to parallel tasks.

---


### Flow of git commit push

First read [Flow of web request](#Flow-of-web-request) above, then pick up the
thread here.

After pushing to a repository, e.g. from the _web UI_:

1. In a web browser, make an edit to a repo file, type a commit message, and
 hit "Commit"
1. NGINX receives the git commit and passes it to Workhorse
1. Workhorse launches a `git-receive-pack` process (on the workhorse machine)
to save the new commit to NFS
1. On the workhorse machine, `git-receive-pack` fires a [git hook](/glossary/#git-hook)
to trigger `GitLab Shell`.
   - GitLab Shell accepts Git payloads pushed over SSH and acts upon them (e.g.
     by checking if you're authorized to perform the push, scheduling the data
     for processing, etc).
   - In this case, GitLab Shell provides the `post-receive` hook, and
the `git-receive-pack` process passes along details of what was pushed to the
repo to the `post-receive` hook. More specifically, it passes a list of three items: old
revision, new revision, and ref (e.g. tag or branch) name.
1. Workhorse then passes the `post-receive` hook to Redis, which is the Sidekiq queue.
   - Workhorse informed that the push succeeded or failed (could have failed due to the repo not available, Redis being down, etc.)
1. Sidekiq picks up the job from Redis and removes the job from the queue
1. Sidekiq updates PostgreSQL
1. Unicorn can now query PostgreSQL.


## Availability and Performance Priority Labels
{: #performance-labels}

To clarify the priority of issues that relate to GitLab.com's availability and
performance consider adding an _Availability and Performance Priority Label_,
`~AP1` through `~AP3`. This is similar to what is in use in the Support and
Security teams, they use [`~SP`](https://about.gitlab.com/handbook/support/workflows/support_workflows/issue_escalations.html#support-priority-labels)
and [`~SL`](https://about.gitlab.com/handbook/infrastructure/security/#security-priority-labels)
labels respectively to indicate priority.

Use the following as a guideline to determine which Availability and Performance
Priority label to use for bugs and feature proposals. Consider the _likelihood_
and _urgency_  of the "scenario" that could result from this issue (not) being
resolved.

- **Urgency:**  _Examples_
 - U1
    - Outage likely within a month.
    - Affects many team members and/or many GitLab.com users
 - U2
    - Outage likely within three months.
    - Affects some team members and/or a few GitLab.com users
 - U3
    - Outage can happen, but not likely in next three months.
    - Affects some team members but no GitLab.com users

- **Impact:** _Examples_
 - I1
    - Outage of >= 25 minutes.
    - Performance improvement (or avoiding degradation) of >= 100 ms expected.
 - I2
    - Outage of 5 - 25 minutes.
    - Performance improvement (or avoiding degradation) of 10-100 ms expected.
 - I3
    - Outage of 0 - 5 minutes.
    - Performance improvement (or avoiding degradation) of <= 10 ms expected.


| **Urgency \ Impact**          | **I1 - High** | **I2 - Medium**  | **I3 - Low**   |
|----------------------------|---------------|------------------|----------------|
| **U1 - High**              | `AP1`         | `AP1`            | `AP2`          |
| **U2 - Medium**            | `AP1`         | `AP2`            | `AP3`          |
| **U3 - Low**               | `AP2`         | `AP3`            | `AP3`          |


## Database Performance

Some general notes about parameters that affect database performance, at a very
crude level.

- From whitebox monitoring,
   - Of time spent on/by Rails controllers, this much is spent in the database: https://performance.gitlab.net/dashboard/db/rails-controllers?orgId=1&panelId=5&fullscreen (for a specific Rails controller / page)
   - _Global_ SQL timings: https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=9&fullscreen&orgId=1&from=now-2d&to=now
- A single HTTP request will execute a single controller. A controller in turn
will usually only use one available database connection, though it may use 2 if
first a read was performed, followed by a write.
   - pgbouncer allows up to 150 concurrent PostgreSQL connections. If this limit
is reached it will block pgbouncer connections until a PostgreSQL
 connection becomes available.
    - PostgreSQL allows up to 300 connections (connected, whether they're active
    or not doesn't matter). Once this limit is reached new connections will be
    rejected, resulting in an error in the application.
    - When the number of processes > number of cores available on the database
    servers, the CPU constantly switches cores to run the requested processes;
    this contention for cores can lead to degraded performance.
- As long as the database CPU load < 100% (http://monitor.gitlab.net/dashboard/db/postgres-stats?refresh=5m&orgId=1&from=now%2Fw&to=now&panelId=13&fullscreen),
then in theory the database can handle more load without adding latency. In
practice database specialists like to keep CPU load below 50%.
    - As an example of how load is determined by underlying application design:
     DB CPU percent used to be lower (20%, prior to 9.2, then up to 50-75% [when
       9.2 RC1 went live](https://gitlab.com/gitlab-org/gitlab-ce/issues/32536),
       then back down to 20% by the time 9.2 was released.
- pgbouncer
   - What it does: pgbouncer maps _N_ incoming connections to _M_ PostreSQL
   connections, with _N_ >= _M_ (_N_ < _M_ would make no sense). For example,
   you can map 1024 incoming connections to 10 PostgreSQL connections. This is mostly influenced by the number of
concurrent queries you want to be able to handle. For example, for GitLab.com
our primary rarely goes above 100 (usually it sits around 20-30), while
secondaries rarely go above 20-30 concurrent queries. The more secondaries you
add, the more you can spread load and thus require fewer connections (at the
  cost of having more servers).
   - Analogy: pgbouncer is a bartender serving drinks to many customers. Instead
   of making the drinks himself she instructs 1 out of 20 “backend” bartenders
   to do so. While one of these bartenders is working on a drink the other 19
   (including the “main” one) are available for new orders. Once a drink is done
   one of the 20 “backend” bartenders gives it to the main bartender, which in
   turn gives it to the customer that requested the drink. In this analogy, the
  _N_ incoming connections are the patrons of the bar, and there are _M_ "backend"
   bartenders.
   - Pgbouncer frontend connections (= incoming ones) are very cheap, and you
   have have lots of these (e.g. thousands). Typically you want _N_ >= _A_ with
   _N_ being the pgbouncer connection limit, and _A_ being the number of
   connections needed for your application.
   - PostgreSQL connections are much more expensive resource wise, and ideally
   you have no more than the number of CPU cores available per server (e.g. 32).
   Depending on your load this may not always be sufficient, e.g. a primary in
   our setup will need to allow 100-150 connections at peak.
   - Pgbouncer can be configured to terminate PostgreSQL connections when idle
   for a certain time period, conserving resources.
